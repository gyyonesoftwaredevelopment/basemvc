﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Directory = Lucene.Net.Store.Directory;
using Version = Lucene.Net.Util.Version;
using Util;
using Newtonsoft.Json;

namespace DynamicDB
{
    public class DBHandler
    {
        private string _datapath { set; get; }
        private string _indexpath { set; get; }

        private string _dataStructureFile
        {
            get { return _datapath + "DataStructure.json";}
        }
        /// <summary>
        /// default data path ./Data
        /// default 
        /// </summary>
        public DBHandler()
        {
            _datapath = @".\Data\";
            _indexpath = @".\Index\";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="datapath">Default ./Data/</param>
        /// <param name="indexpath">Default ./Index/</param>
        public DBHandler(string datapath, string indexpath)
        {
            _datapath = datapath;
            _indexpath = indexpath;
        }

        public void Save(DataTable dt)
        {
            if (String.IsNullOrWhiteSpace(dt.Id))
            {
                dt.Id = Guid.NewGuid().ToString("N");
            }
            PopulateId(dt, "");
            CreateIndex(dt);
            WriteData(dt);
        }

        private void PopulateId(DataTable dt,string id)
        {
            foreach (var dataRow in dt.Rows)
            {
                if (String.IsNullOrWhiteSpace(dataRow.Id))
                {
                    dataRow.Id = Guid.NewGuid().ToString("N");
                }
                dataRow.RefId = id;
                if (dataRow.Details.Count > 0)
                {
                    foreach (DataTable dataTable in dataRow.Details)
                    {
                        PopulateId(dataTable, dataRow.Id);
                    }
                }
            }
        }

        public void WriteData(DataTable dt,DataTable userconfig = null)
        {
            //Check for Permission to change or add file - Pending


            string path = _datapath + dt.TableName + "\\";
            DirectoryHelper.CreateDirectory(path);
            foreach (DataRow dataRow in dt.Rows)
            {
                using (StreamWriter sw = new StreamWriter(path + dataRow.Id + ".json"))
                {
                    sw.Write(JsonConvert.SerializeObject(dataRow));
                }
                if (dataRow.Details.Count > 0)
                {
                    WriteDetailData(dataRow.Details, path, userconfig);
                }
            }
        }

        public void SaveDbStructure(DataStructureDictionary datastructure)
        {
            DirectoryHelper.CreateDirectory(_dataStructureFile);
            using (StreamWriter sw = new StreamWriter(_dataStructureFile))
            {
                sw.Write(JsonConvert.SerializeObject(datastructure));
            }
        }

        public DataStructureDictionary GetDbStructure()
        {
            DataStructureDictionary _temp = null;
            using (StreamReader sr = new StreamReader(_dataStructureFile))
            {
                _temp =JsonConvert.DeserializeObject<DataStructureDictionary>(sr.ReadToEnd());
            }
            return _temp?? new DataStructureDictionary();
        }

        public void WriteDetailData(List<DataTable> dataTables, string datapath, DataTable userconfig = null)
        {
             
            foreach (var dt in dataTables)
            {
                string path = datapath + dt.TableName + "\\";
                foreach (DataRow dataRow in dt.Rows)
                {
                    DirectoryHelper.CreateDirectory(path);
                    using (StreamWriter sw = new StreamWriter(path + dataRow.Id + ".json"))
                    {
                        sw.Write(JsonConvert.SerializeObject(dataRow));
                    }
                    if (dataRow.Details.Count > 0)
                    {
                        WriteDetailData(dataRow.Details, path, userconfig);
                    }
                }
            }
        }

        private Field.Index GetIndex(DataTable userconfig)
        {
            //Check for user setting on UI  - Pending
            //userconfig.Details.Where(x => x.TableName == "UserConfig");
            if (userconfig == null)
            {
                return Field.Index.ANALYZED;
            }
            return Field.Index.ANALYZED;

        }

        private void CreateIndex(DataTable data,DataTable userconfig = null)
        {
            Analyzer analyser = new StandardAnalyzer(Version.LUCENE_30);
            Directory directory = FSDirectory.Open(new DirectoryInfo(_indexpath + data.TableName));
            var writer = new IndexWriter(directory, analyser, true, IndexWriter.MaxFieldLength.LIMITED);
            foreach (var dataRow in data.Rows)
            {

                var doc = new Document();
                doc.Add(new Field("Id", dataRow.Id, Field.Store.YES, Field.Index.NOT_ANALYZED));
                if (!string.IsNullOrEmpty(dataRow.RefId))
                {
                    doc.Add(new Field("RefId", dataRow.RefId, Field.Store.YES, Field.Index.NOT_ANALYZED));
                }
                foreach (var column in data.Columns)
                {
                    doc.Add(new Field(column, dataRow[column], Field.Store.YES, GetIndex(userconfig)));
                    if (dataRow.Details.Count > 0)
                    {
                        CreateDetailIndex(dataRow.Details,userconfig, _indexpath + data.TableName + "\\");
                    }
                }

                writer.AddDocument(doc);
                

            }
            writer.Optimize();
            writer.Dispose(false);

            //IndexReader indexReader = IndexReader.Open(directory ,true);
            //Searcher indexsearch = new IndexSearcher(indexReader);


            //QueryParser queryparser = new QueryParser(Version.LUCENE_29, "Supplier", analyser);
            //Query fuzzyquery = new FuzzyQuery(new Term("Supplier", "long"),0.01f);

            //var query = queryparser.Parse("\"long\"~10000");

            //TopDocs resultDocs = indexsearch.Search(fuzzyquery, indexReader.MaxDoc);

            //var hits = resultDocs.ScoreDocs;
            //foreach (var hit in hits)
            //{
            //    var documentfromsarch = indexsearch.Doc(hit.Doc);
            //    MessageBox.Show(documentfromsarch.Get("Contact"));
            //}
        }

        private void CreateDetailIndex(List<DataTable> dataTable, DataTable userconfig, string tablePath)
        {
            foreach (var data in dataTable)
            {
                Analyzer analyser = new StandardAnalyzer(Version.LUCENE_30);
                Directory directory = FSDirectory.Open(new DirectoryInfo(tablePath + data.TableName));
                var writer = new IndexWriter(directory, analyser, true, IndexWriter.MaxFieldLength.LIMITED);
                foreach (var dataRow in data.Rows)
                {
                    var doc = new Document();
                    doc.Add(new Field("Id", dataRow.Id, Field.Store.YES, Field.Index.NOT_ANALYZED));
                    if (!string.IsNullOrEmpty(dataRow.RefId))
                    {
                        doc.Add(new Field("RefId", dataRow.RefId, Field.Store.YES, Field.Index.NOT_ANALYZED));
                    }
                    foreach (var column in data.Columns)
                    {
                        doc.Add(new Field(column, dataRow[column], Field.Store.YES, GetIndex(userconfig)));
                        if (dataRow.Details.Count > 0)
                        {
                            CreateDetailIndex(dataRow.Details,userconfig, tablePath + data.TableName + "\\");
                        }
                    }

                    writer.AddDocument(doc);
                }
                writer.Optimize();
                writer.Dispose(false);
            }

        }




    }
}