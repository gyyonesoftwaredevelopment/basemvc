﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DynamicDB
{
    public class DataStructureDictionary
    {
        //Provide Table Name get field list, Table Name,FieldName get Permmision
        public Dictionary<string,Dictionary<string,Permission>> TableToFieldsDic { set; get; }
        //Table Name Get Permission
        public Dictionary<string, Permission> TablePermission { set; get; }

        public TreeNode Menu { set; get; }
    }
}
