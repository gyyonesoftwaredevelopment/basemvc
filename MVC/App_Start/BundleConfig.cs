﻿using System.Web;
using System.Web.Optimization;

namespace MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            BundleJquery(bundles);
            BundleBootstrap(bundles);
            BundleTheme(bundles);
            BundleSimpleLineIcons(bundles);
            BundleJsTree(bundles);
            BundleSelect2(bundles);
            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }

        private static void BundleJquery(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/jquery").Include(
              "~/Bundles/jquery/jquery.js",
              "~/Bundles/jquery/jquery-migrate.js",
              "~/Bundles/jquery/jquery-ui.js",
                "~/Bundles/jquery/jquery.slimscroll.js",
                "~/Bundles/jquery/jquery.blockui.js",
                "~/Bundles/jquery/jquery.cokie.js"));
              
            bundles.Add(new StyleBundle("~/jquerycss").Include(
                                  "~/Bundles/jquery/jquery-ui.css"));
        }
        private static void BundleUniform(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/uniform").Include(
            "~/Bundles/uniform/jquery.uniform.js"));



            bundles.Add(new StyleBundle("~/uniformcss").Include(
                                  "~/Bundles/uniform/css/uniform.default.css"));
        }

        private static void BundleBootstrap(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bootstrap").Include(
            "~/Bundles/bootstrap/bootstrap.js",
             "~/Bundles/bootstrap/bootstrap-hover-dropdown.js",
               "~/Bundles/bootstrap/bootstrap-switch.js"));

            bundles.Add(new StyleBundle("~/bootstrapcss").Include(
                                  "~/Bundles/bootstrap/bootstrap.css",
                                  "~/Bundles/bootstrap/bootstrap-switch.css"));
        }

        private static void BundleTheme(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/theme").Include(
            "~/Bundles/theme/metronic.js",
            "~/Bundles/theme/app.js",
             "~/Bundles/theme/datatable.js",
            "~/Bundles/theme/layout.js"));

            bundles.Add(new StyleBundle("~/themecss").Include(
                                 "~/Bundles/theme/font-awesome.css",
                                 "~/Bundles/theme/components.css",
                                 "~/Bundles/theme/plugins.css",
                                 "~/Bundles/theme/layout.css",
                                 "~/Bundles/theme/default.css",
                                 "~/Bundles/theme/darkblue.css",
                                 "~/Bundles/theme/custom.css"));

     
        }

        private static void BundleSimpleLineIcons(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/simplelineiconscss").Include(
                                 "~/Bundles/simple-line-icons/simple-line-icons.css"
                         ));
        }
        private static void BundleJsTree(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/jstree").Include(
                "~/Bundles/jstree/jstree.js"));

            bundles.Add(new StyleBundle("~/jstreecss").Include(
                                 "~/Bundles/jstree/themes/default/style.css"));
        }

        private static void BundleSelect2(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/select2").Include(
                "~/Bundles/select2/select2.js",
                 "~/Bundles/select2/select2Init.js"));

            bundles.Add(new StyleBundle("~/select2css").Include(
                                 "~/Bundles/select2/select2.css",
                                 "~/Bundles/select2/select2-bootstrap.min.css"));
        }
    }
}
