﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Akka.Actor;
using MVC.Actor;
using MVC.Models;
using Util;

namespace MVC.Controllers
{
    public class DataController : Controller
    {
        // GET: Data
        [HttpPost]
        public JsonResult Save( )
        {
            return Json("Ok");
        }

        [HttpPost]
        public JsonResult Update()
        {
            return Json("Ok");
        }

        [HttpPost]
        public JsonResult Delete()
        {
            return Json("Ok");
        }

        [HttpPost]
        public JsonResult Select(string search, string tableName, string fieldName)
        {
            //
            //
            DataTable dt = new DataTable("Test");
            SystemActors.SignalRActor.Tell(dt, ActorRefs.Nobody);
            List<DrowDownItem> _DropdownList = new List<DrowDownItem>();
            _DropdownList.Add(new DrowDownItem() { id = "A", text = "Apple" + Session["Token"] });
            _DropdownList.Add(new DrowDownItem() { id = "B", text = "Boy" + Session["Token"] });
          return Json(_DropdownList);
        }

        [HttpPost]
        public ActionResult SendRequest()
        {
            DataTable dt = new DataTable("Test");
            SystemActors.SignalRActor.Tell(dt, ActorRefs.Nobody);
            return new EmptyResult();
        }
    }
}