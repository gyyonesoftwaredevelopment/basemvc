﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Util;
using MVC.Models;
using MVC.Properties;
using MVC.Security;
using Newtonsoft.Json;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["Token"] = "ABCDEFG";
            UserSetting usersetting = new UserSetting();
            usersetting.Menu = new TreeNode("Data Structure");
         
            usersetting.UserId = "Development";
            return RedirectToAction("Index", "Menu");
            //return View(usersetting);
        }
     
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}