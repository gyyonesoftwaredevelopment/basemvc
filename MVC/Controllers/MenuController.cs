﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using Util;
namespace MVC.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        // GET: Data
        [HttpPost]
        public JsonResult Save( Dictionary<string, string> valueDic)
        {
            return Json("Ok");
        }

        public ActionResult Index()
        {
            UserSetting usersetting = new UserSetting();
            usersetting.Menu = new TreeNode("Test");
            List<ControlModel> _controls = new List<ControlModel>();
            _controls.Add(new InputModel() { Label = "Testlabel", Value = "Test Label", PlaceHolder = "Please enter key", Icon = "fa fa-user" });
            _controls.Add(new InputModel() { Label = "Testlabel", Value = "Test Label", PlaceHolder = "Please enter key", Icon = "fa fa-user" });
            _controls.Add(new InputModel() { Label = "Testlabel", Value = "Test Label", PlaceHolder = "Please enter key", Icon = "fa fa-user" });
            _controls.Add(new DropdownModel(){Label = "Test",Value = "Test",FieldName = "Test",TableName ="Test",SelectMode = "select2"});
            ViewBag.Controls = _controls;
            //sersetting.Menu = new TreeNode("Test");
            return View("Menu", usersetting);
        }
    }
}