﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Akka.Actor;
using Akka.Routing;
using MVC.Actor;


namespace MVC
{

    public class MvcApplication : System.Web.HttpApplication
    {



        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //your mvc config. Does not really matter if you initialise



            ActorSystemRefs.ActorSystem = ActorSystem.Create("webcrawler");
            var actorSystem = ActorSystemRefs.ActorSystem;
            var router = actorSystem.ActorOf(Props.Empty
                .WithRouter(FromConfig.Instance), "tasker");

            SystemActors.SignalRActor = actorSystem.ActorOf(Props.Create(() =>
                new DataHandler.DataHandler()), "signalr");
        }


        protected void Application_End()
        {
            ActorSystemRefs.ActorSystem.Shutdown();

            //wait up to two seconds for a clean shutdown
            ActorSystemRefs.ActorSystem.AwaitTermination(TimeSpan.FromSeconds(2));
        }
    }

}