﻿using System;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class ControlModel
    {
        public string Label { set; get; }
        public string Name { set; get; }
        public string Value { set; get; }
        public virtual string LabelColMd { set; get; }
        public virtual string ValueColMd { set; get; }
  
    }
}