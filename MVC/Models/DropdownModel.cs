﻿using System.Collections.Generic;

namespace MVC.Models
{
    public class DropdownModel : ControlModel
    {
        //select2-multiple, select2
        public string SelectMode { set; get; }
        public string TableName { set; get; }
        public string FieldName { set; get; }
       

    }

    public class DrowDownItem
    {
        public string id { set; get; }
        public string text { set; get; }
        public string ItemKey { set; get; }
        public string ItemValue { set; get; }
    }
}