namespace MVC.Models
{
    public class InputModel : ControlModel
    {
        public override string LabelColMd
        {
            set { base.LabelColMd = value; }
            get { return base.LabelColMd = base.LabelColMd ?? "col-md-3"; }

        }

        public override string ValueColMd
        {
            set { base.ValueColMd = value; }
            get { return base.ValueColMd = base.LabelColMd ?? "col-md-4"; }

        }

        public string Icon { set; get; }
        public string PlaceHolder { set; get; }
    }
}