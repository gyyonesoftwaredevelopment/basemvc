﻿using System.Collections.Generic;
using Util;


namespace MVC.Models
{
    public class UserSetting
    {
        public string UserId { set; get; }
        public string Language { set; get; }
        public TreeNode Menu { set; get; }

      
    }
}