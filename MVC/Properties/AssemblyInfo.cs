﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Raven.Imports.Newtonsoft.Json;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MVC")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("MVC")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("21ce05e2-6417-4d71-acf4-4bc95184e632")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
public static class AssemblyCollection
{
    public static List<Type> GetTypes(string assemblyNameSpace)
    {
        List<Type> temp = new List<Type>();

        foreach (Type type in GetTypesInNamespace(Assembly.GetExecutingAssembly(), assemblyNameSpace))
        {
            temp.Add(type);
        }
        return temp;
    }
    public static List<PropertyInfo> GetProperty(string assemblyNameSpace, string classname)
    {
        List<PropertyInfo> temp = new List<PropertyInfo>();
        Type[] typecollection = GetTypesInNamespace(Assembly.GetExecutingAssembly(), assemblyNameSpace).Where(x => x.FullName.Equals(classname)).ToArray();
        foreach (var type in typecollection)
        {
            foreach (var propertyInfo in type.GetProperties())
            {
                object[] attrs = propertyInfo.GetCustomAttributes(typeof(JsonIgnoreAttribute), true);
                if (attrs.Length == 0 && propertyInfo.GetSetMethod(true).IsPublic)
                {
                    temp.Add(propertyInfo);
                }
                
            }
        }
        return temp;

    }

    /// <summary>
    /// Return type of the object. Example List<SalesOrder>  Will return SalesOrderType instead of List. SalesOrder[] will return SalesOrder.
    /// </summary>
    /// <param name="propertyInfo"></param>
    /// <returns></returns>
    public static Type GetTypeWithoutCollection(PropertyInfo propertyInfo)
    {
        if (propertyInfo.PropertyType.IsArray)
        {
            return propertyInfo.PropertyType.GetElementType();
        }
        else if (propertyInfo.PropertyType.IsConstructedGenericType)
        {
            return propertyInfo.PropertyType.GetGenericArguments()[0];
        }
        else
        {
            return propertyInfo.PropertyType;
        }
    }

    public static List<string> GetPropertyName(string assemblyNameSpace, string classname)
    {
        List<string> temp = new List<string>();
        Type[] typecollection = GetTypesInNamespace(Assembly.GetExecutingAssembly(), assemblyNameSpace).Where(x => x.FullName.Equals(classname)).ToArray();
        foreach (var type in typecollection)
        {
            foreach (var propertyInfo in type.GetProperties())
            {
                object[] attrs = propertyInfo.GetCustomAttributes(typeof(JsonIgnoreAttribute), true);
                if (attrs.Length == 0 && propertyInfo.GetSetMethod(true).IsPublic)
                {
                    temp.Add(propertyInfo.Name);
                }
            }
        }
        return temp;

    }

    private static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
    {
   
       //return _types.ToArray();
       return assembly.GetTypes().Where(t => !String.IsNullOrEmpty(t.Namespace) && t.Namespace.ToUpper().Contains(nameSpace.ToUpper())).ToArray();
    }


}