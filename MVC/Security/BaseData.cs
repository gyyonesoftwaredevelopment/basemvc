﻿using System;
using System.Linq;
using System.Web;

namespace MVC.Security
{
    public enum ControlType
    {
        Input,
        CheckBox,
        DropdownList,
        TypeAhead,
        Label,
    }
    public class BaseData
    {
        [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        protected string ClassName
        {
            get { return this.GetType().Name; }
        }
        public virtual string Id { set; get; }

        [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        private DateTime _createdOn { set; get; }
        public DateTime CreatedOn
        {
            set { _createdOn = value; }
            get
            {
                if (_createdOn == default(DateTime))
                {
                    _createdOn = DateTime.Now;
                }
                return _createdOn;
            }
        }
  
        public DateTime UpdatedOn
        {
            get { return DateTime.Now; }
        }

        public bool IsArchived { set; get; }

    }

 
}