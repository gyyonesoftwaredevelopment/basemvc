using System.Collections.Generic;

namespace MVC.Security
{
    public class ClassPermission : Permission
    {
        public string FullName { set; get; }
        public string Url { set; get; }


        [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        private List<PropertyPermission> _propertyList { set; get; }
        public List<PropertyPermission> PropertyList
        {
            set { _propertyList = (_propertyList == null ? new List<PropertyPermission>() : value); }
            get { return _propertyList = (_propertyList ?? new List<PropertyPermission>()); }
        }
    }
}