using System.Collections.Generic;

namespace MVC.Security
{
    public class Permission : BaseData
    {
        public Permission()
        {
            Enable = true;
            Icon = "fa fa-folder icon-state-warning";


        }
        [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        public string PropertyName
        {
            get { return ClassName; }
        }
        public string Name { set; get; }
        public string Text { set; get; }


        public bool Enable { set; get; }

        //Only Show for RoleList is add in
        [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        private List<string> _roleslist { set; get; }

        public List<string> RoleList
        {
            set { _roleslist = (_roleslist == null ? new List<string>() : value); }
            get { return _roleslist = (_roleslist ?? new List<string>()); }
        }

        public string Icon { set; get; }

    }
}