namespace MVC.Security
{
    public class PropertyPermission : Permission
    {
        [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        private UserControlSource _userControlSource { set; get; }
        public UserControlSource UserControl
        {
            set { _userControlSource = value; }
            get { return _userControlSource = _userControlSource ?? new UserControlSource(); }
        }


        [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        public string ParentName { set; get; }


    }
}