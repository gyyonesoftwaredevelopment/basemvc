﻿using System.Collections.Generic;

namespace MVC.Security
{
    public class Rolelookup : BaseData
    {

        public string RoleName { set; get; }

         [Raven.Imports.Newtonsoft.Json.JsonIgnore]
        private List<string> _roleList { set; get; }


        public List<string> SubRoleList
        {
            set { _roleList = (_roleList == null ? new List<string>() : value); }
            get { return _roleList = (_roleList ?? new List<string>()); }
        }
    }
}