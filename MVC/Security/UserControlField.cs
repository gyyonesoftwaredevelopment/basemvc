﻿namespace MVC.Security
{
    public class UserControlField
    {
        public string FieldName { set; get; }
        public bool Enable { set; get; }

    }
}