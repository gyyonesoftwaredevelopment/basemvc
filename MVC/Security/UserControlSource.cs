using System.Collections.Generic;

namespace MVC.Security
{
    public class UserControlSource
    {
        public UserControlSource()
        {
            Controltype = ControlType.Input.ToString();
            ClassName = "";
            FieldDesc = new List<UserControlField>();
        }
        public string Controltype { set; get; }

        public string ClassName { set; get; }

        //Usually Id, must be unique
        //public string PrimaryKey { set; get; }
        public string PrimaryKey { get { return "Id"; } }
        //Display Only, Not for Save
        public List<UserControlField> FieldDesc { set; get; }

    }
}